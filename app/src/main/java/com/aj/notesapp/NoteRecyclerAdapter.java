package com.aj.notesapp;

import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;

//fireStoreRecyclerAdapter takes Notes pojo class and View holder arguments
class NoteRecyclerAdapter  extends FirestoreRecyclerAdapter<Note, NoteRecyclerAdapter.NoteViewHolder> {

    private static final String TAG = "NoteRecyclerAdapter";
    NoteInterface noteInterface;

    public NoteRecyclerAdapter(@NonNull FirestoreRecyclerOptions<Note> options,NoteInterface noteInterface) {
        super(options);

        this.noteInterface = noteInterface;
    }

    @Override
    protected void onBindViewHolder(@NonNull NoteViewHolder holder, int position, @NonNull Note model) {

        holder.noteTextView.setText(model.getText());
        holder.checkBox.setChecked(model.getCompleted());

       // CharSequence dateCharSequence = DateFormat.format("EEEE,MMM,d,yyyy, h:mm:ss,a");
        CharSequence dateCharSequence = DateFormat.format("EEEE,MMM d,yyyy,h:mm:ss a",model.getCreated_at().toDate());

        holder.dateTextView.setText(dateCharSequence);
    }

    @NonNull
    @Override
    public NoteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.note_row,parent,false);
        return new NoteViewHolder(view);
    }

    class NoteViewHolder extends RecyclerView.ViewHolder
    {

        TextView noteTextView,dateTextView;
        CheckBox checkBox;

        public NoteViewHolder(@NonNull View itemView) {
            super(itemView);

            noteTextView = itemView.findViewById(R.id.noteTextView);
            dateTextView = itemView.findViewById(R.id.dateTextView);
            checkBox = itemView.findViewById(R.id.checkBox);


            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                    DocumentSnapshot snapshot = getSnapshots().getSnapshot(getAdapterPosition());

                    Note note = getItem(getAdapterPosition());

                    //if we not checked checkbox only then we want to handle changes in firestore
                    if (note.getCompleted() != isChecked)
                    {
                        noteInterface.handleCheckChanged(isChecked,snapshot);
                    }

                }
            });


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    DocumentSnapshot snapshot = getSnapshots().getSnapshot(getAdapterPosition());

                    noteInterface.handleEditNote(snapshot);
                }
            });

        }

        public void deleteItem()
        {
//            Log.d(TAG, "deleteItem: "+getAdapterPosition());
//            Log.d(TAG, "deleteItem: "+getSnapshots().getSnapshot(getAdapterPosition()));
            noteInterface.handleDeleteItem(getSnapshots().getSnapshot(getAdapterPosition()));

        }
    }

    interface NoteInterface
    {
        public void handleCheckChanged(boolean isChecked, DocumentSnapshot snapshot);
        public void handleEditNote(DocumentSnapshot snapshot);
        public void handleDeleteItem(DocumentSnapshot snapshot);
    }
}
