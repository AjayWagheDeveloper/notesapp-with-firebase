package com.aj.notesapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.Date;

import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator;

public class MainActivity extends AppCompatActivity implements FirebaseAuth.AuthStateListener, NoteRecyclerAdapter.NoteInterface {

    RecyclerView recyclerView;
    FirebaseFirestore firestore;
    NoteRecyclerAdapter noteRecyclerAdapter;
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firestore = FirebaseFirestore.getInstance();

        recyclerView = findViewById(R.id.recyclerView);

        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Here's a Snackbar", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();

                //startActivity(new Intent(MainActivity.this,FirestoreExamples.class));

                showAlertDialog();

            }
        });


        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            startLoginActivity();
        }
    }

    private void showAlertDialog() {

        final EditText editText = new EditText(this);

        new AlertDialog.Builder(this)
                .setTitle("Create Note")
                .setView(editText)
                .setPositiveButton("add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

//                        Toast.makeText(MainActivity.this, "added new note..", Toast.LENGTH_SHORT).show();
//                        Toast.makeText(MainActivity.this, editText.getText(), Toast.LENGTH_SHORT).show();

                        addNote(editText.getText().toString());
                    }
                })
                .setNegativeButton("cancel", null)
                .show();
    }


    private void addNote(String text) {
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();

        Note note = new Note(text, false, new Timestamp(new Date()), userId);

        firestore.collection("notes")
                .add(note)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {

                        Log.d(TAG, "onSuccess: created new note");
                    }
                })

                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        Log.d(TAG, "onFailure: failed to create note");
                    }
                });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main_menu, menu);


        return super.onCreateOptionsMenu(menu);


    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.profile:
                Toast.makeText(this, "profile", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(MainActivity.this,ProfileActivity.class));
                return true;
            case R.id.logout:
                //logOut a user using AuthUI instance
                AuthUI.getInstance().signOut(this);

                Toast.makeText(this, "logOut", Toast.LENGTH_SHORT).show();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    //Login activity intent method
    private void startLoginActivity() {
        Intent intent = new Intent(this, LoginRegisterActivity.class);
        startActivity(intent);
        finish();
    }


    //* code starts here
    // auth state listener , we will start it on start and close it on stop
    @Override
    protected void onStart() {
        super.onStart();

        FirebaseAuth.getInstance().addAuthStateListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        FirebaseAuth.getInstance().removeAuthStateListener(this);

        //stop recycler listener

        if (noteRecyclerAdapter != null) {
            noteRecyclerAdapter.stopListening();
        }
    }

    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

        //if null user then start loging activity and return /stop
        if (firebaseAuth.getCurrentUser() == null) {
            startLoginActivity();
            return;
        }

        initRecyclerView(firebaseAuth.getCurrentUser());

        //or listen state
//        firebaseAuth.getCurrentUser().getIdToken(true)
//                .addOnSuccessListener(new OnSuccessListener<GetTokenResult>() {
//                    @Override
//                    public void onSuccess(GetTokenResult getTokenResult) {
//
//                        //Test // logging jwt token
//                        Log.d(TAG, "onSuccess: " + getTokenResult.getToken());
//                    }
//                });
//
//        Log.d(TAG, "onAuthStateChanged: "+firebaseAuth.getCurrentUser().getPhoneNumber());

    }

    //*code ends here


    //recylerview

    private void initRecyclerView(FirebaseUser user) {

        Query query = FirebaseFirestore.getInstance()
                .collection("notes")
                .whereEqualTo("userId", user.getUid())
                .orderBy("completed", Query.Direction.ASCENDING)
                .orderBy("created_at", Query.Direction.DESCENDING);

        FirestoreRecyclerOptions<Note> options = new FirestoreRecyclerOptions.Builder<Note>()
                .setQuery(query, Note.class)
                .build();


        noteRecyclerAdapter = new NoteRecyclerAdapter(options, this);
        recyclerView.setAdapter(noteRecyclerAdapter);

        noteRecyclerAdapter.startListening();

        //attaching touch helper to recyclerview

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);

        itemTouchHelper.attachToRecyclerView(recyclerView);


    }

    ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.LEFT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

            if (direction == ItemTouchHelper.LEFT)
            {
                Toast.makeText(MainActivity.this, "deleted", Toast.LENGTH_SHORT).show();
            }

            NoteRecyclerAdapter.NoteViewHolder noteViewHolder = (NoteRecyclerAdapter.NoteViewHolder) viewHolder;

            noteViewHolder.deleteItem();

        }

        @Override
        public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

            new RecyclerViewSwipeDecorator.Builder(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                    .addBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorAccent))
                    .addActionIcon(R.drawable.ic_baseline_delete_24)
                    .create()
                    .decorate();

            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        }
    };


    @Override
    public void handleCheckChanged(final boolean isChecked, DocumentSnapshot snapshot) {

        //checkBox handle interface method
        Log.d(TAG, "handleCheckChanged: " + isChecked);

        snapshot.getReference().update("completed", isChecked)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        Log.d(TAG, "onSuccess: Updated isChecked " + isChecked);
                    }
                })

                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        Log.d(TAG, "onFailure: " + e.getLocalizedMessage());
                    }
                });
    }

    @Override
    public void handleEditNote(final DocumentSnapshot snapshot) {
        //edit note handle interface method

        //getting text from snapshot
        final Note note = snapshot.toObject(Note.class);

        final EditText editText = new EditText(this);

        //set text to edit text
        editText.setText(note.getText());

        //move cursor of edit text to after text,which is before text
        editText.setSelection(note.getText().length());

        new AlertDialog.Builder(this)
                .setTitle("Edit Note")
                .setView(editText)
                .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        String newText = editText.getText().toString();

                        //*****
                        //this is another way of updating data in firestore
                        //with note object and setting note
                        note.setText(newText);
                        snapshot.getReference().set(note)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {

                                        Log.d(TAG, "onSuccess: edited success..");
                                    }
                                });
                        //*******
                    }
                })

                .setNegativeButton("Cancel", null)
                .show();
    }

    @Override
    public void handleDeleteItem(DocumentSnapshot snapshot) {

        final DocumentReference documentReference = snapshot.getReference();
        final Note note = snapshot.toObject(Note.class);

      documentReference.delete()
              .addOnSuccessListener(new OnSuccessListener<Void>() {
                  @Override
                  public void onSuccess(Void aVoid) {

                      Toast.makeText(MainActivity.this, "Item Deleted", Toast.LENGTH_SHORT).show();
                  }
              });

      Snackbar.make(recyclerView,"Item Deleted",Snackbar.LENGTH_LONG)
              .setAction("Undo", new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {

                      documentReference.set(note);
                  }
              }).show();

    }
}