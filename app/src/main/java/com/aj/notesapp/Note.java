package com.aj.notesapp;

import com.google.firebase.Timestamp;

class Note {

    private String text;
    private boolean completed;
    private Timestamp created_at;
    private String userId;

    public Note() {
    }

    public Note(String text, boolean completed, Timestamp created_at, String userId) {
        this.text = text;
        this.completed = completed;
        this.created_at = created_at;
        this.userId = userId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean getCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public Timestamp getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Timestamp created_at) {
        this.created_at = created_at;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
