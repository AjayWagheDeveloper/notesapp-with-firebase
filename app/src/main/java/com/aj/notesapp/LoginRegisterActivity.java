package com.aj.notesapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;
import java.util.List;

public class LoginRegisterActivity extends AppCompatActivity  {

    private static final String TAG = "LoginRegisterActivity";

    int AUTH_UI_REQUEST_CODE = 1001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_register);

        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            startActivity(new Intent(this, MainActivity.class));
            this.finish();
        }
    }

    public void handleLoginRegister(View view) {


        List<AuthUI.IdpConfig> provider = Arrays.asList(
                //email password login
                new AuthUI.IdpConfig.EmailBuilder().build(),
                //google auth login
                new AuthUI.IdpConfig.GoogleBuilder().build(),
                //phone number login
                new AuthUI.IdpConfig.PhoneBuilder().build()

        );


        Intent intent = AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setAvailableProviders(provider)
                .setLogo(R.drawable.notes)
                .setTosAndPrivacyPolicyUrls("www.example.com","www.example.com/policy")
                .build();


        startActivityForResult(intent, AUTH_UI_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AUTH_UI_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                //we have signed in a user or we have new user
                //we will check is user is new or already signed in

                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                Log.d(TAG, "onActivityResult: " + user.getEmail());

                if (user.getMetadata().getCreationTimestamp() == user.getMetadata().getLastSignInTimestamp()) {
                    //this means new user
                    Toast.makeText(this, "Welcome New User", Toast.LENGTH_SHORT).show();
                } else {
                    //else already existing user
                    Toast.makeText(this, "Welcome Back Again..", Toast.LENGTH_SHORT).show();
                }

                Intent intent = new Intent(this,MainActivity.class);
                startActivity(intent);
                this.finish();


            } else {
                //if signed in is failed or canceled by user check

                IdpResponse response = IdpResponse.fromResultIntent(data);

                if (response == null) {
                    Log.d(TAG, "onActivityResult: " + "User has canceled the sign in request");
                } else {
                    Log.e(TAG, "onActivityResult: " + response.getError());
                }
            }
        }
    }
}