package com.aj.notesapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {

    private static final String TAG = "ProfileActivity";
   int TAKE_IMAGE_CODE = 10025;

    CircleImageView circleImageView;
    TextInputEditText displayNameEdttext;
    Button updateProfileBtn;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        circleImageView = findViewById(R.id.circleImageView);
        displayNameEdttext = findViewById(R.id.displayNameEdtText);
        updateProfileBtn = findViewById(R.id.updateProfileBtn);
        progressBar = findViewById(R.id.progressBar);


        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();



        if (user != null) {
            Log.d(TAG, "onCreate: " + user.getDisplayName());

            if (user.getDisplayName() != null) {
                displayNameEdttext.setText(user.getDisplayName());
                displayNameEdttext.setSelection(user.getDisplayName().length());
            }
            if (user.getPhotoUrl() !=null)
            {
                Glide.with(this).load(user.getPhotoUrl()).into(circleImageView);
            }
        }

        progressBar.setVisibility(View.GONE);
    }

    public void updateProfile(final View view) {

        //disable update button
        view.setEnabled(false);

        //set active progress bar
        progressBar.setVisibility(View.VISIBLE);

        String New_Display_Name = displayNameEdttext.getText().toString();

        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        UserProfileChangeRequest request = new UserProfileChangeRequest.Builder()
                .setDisplayName(New_Display_Name)
                .build();

        firebaseUser.updateProfile(request)

                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {


                        //enable update button
                        view.setEnabled(false);
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(ProfileActivity.this, "profile updated success !", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        //enable update button
                        view.setEnabled(false);
                        progressBar.setVisibility(View.GONE);
                        Log.d(TAG, "onFailure: " + e.getLocalizedMessage());
                    }
                });

    }

    public void upload_file_to_storage(View view) {

//        FirebaseStorage storage = FirebaseStorage.getInstance();
//
//        StorageReference reference = storage.getReference();
//        StorageReference imgRef = reference.child("profile_images").child("img1.jpg");

        //upload image
//        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(),R.drawable.dawn);
//
//        ByteArrayOutputStream stream = new ByteArrayOutputStream();
//
//        imageBitmap.compress(Bitmap.CompressFormat.JPEG,20,stream);
//
//        imgRef.putBytes(stream.toByteArray())
//                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//                    @Override
//                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//
//                        Log.d(TAG, "onSuccess: upload success...");
//                        Toast.makeText(ProfileActivity.this, "Uploaded Image", Toast.LENGTH_SHORT).show();
//                    }
//                })
//                .addOnFailureListener(new OnFailureListener() {
//                    @Override
//                    public void onFailure(@NonNull Exception e) {
//                        Log.d(TAG, "onFailure: failed...");
//                        Toast.makeText(ProfileActivity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
//                    }
//                });


        //get image

//        imgRef.getBytes(1024*1024)
//                .addOnSuccessListener(new OnSuccessListener<byte[]>() {
//                    @Override
//                    public void onSuccess(byte[] bytes) {
//
//                        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
//                        circleImageView.setImageBitmap(bitmap);
//                    }
//                });


        //get download url

//        imgRef.getDownloadUrl()
//                .addOnSuccessListener(new OnSuccessListener<Uri>() {
//                    @Override
//                    public void onSuccess(Uri uri) {
//
//                        Log.d(TAG, "onSuccess: url : "+ uri.toString());
//                    }
//                });

    }

    public void handleProfileImage(View view) {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, TAKE_IMAGE_CODE);


        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == TAKE_IMAGE_CODE)
        {
            switch (resultCode)
            {
                case RESULT_OK :
                    Toast.makeText(this, "ok result", Toast.LENGTH_SHORT).show();
                    Bitmap bitmap = (Bitmap) data.getExtras().get("data");

                    circleImageView.setImageBitmap(bitmap);

                    handleUploadProfileImage(bitmap);

                case RESULT_CANCELED:
                    Toast.makeText(this, "result cancelled ..", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void handleUploadProfileImage(Bitmap bitmap) {

        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        bitmap.compress(Bitmap.CompressFormat.JPEG,100,stream);

        String userID = FirebaseAuth.getInstance().getCurrentUser().getUid();

        final StorageReference storageReference = FirebaseStorage.getInstance().getReference()
                                             .child("profile_images")
                                             .child(userID +".jpeg");


        storageReference.putBytes(stream.toByteArray())
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        Log.d(TAG, "onSuccess: upload profile image sucess...");
                        Toast.makeText(ProfileActivity.this, "uploaded", Toast.LENGTH_SHORT).show();

                        getDownloadUrl(storageReference);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        Log.d(TAG, "onFailure: "+e.getCause());
                        Toast.makeText(ProfileActivity.this, ""+e.getCause(), Toast.LENGTH_SHORT).show();
                    }
                });



    }

    private void getDownloadUrl(StorageReference reference) {

        reference.getDownloadUrl()
                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {

                        Log.d(TAG, "onSuccess: "+uri);

                        setUserProfileUri(uri);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        Log.d(TAG, "onFailure: "+e.getCause());
                    }
                });
    }

    private void setUserProfileUri(Uri uri)
    {
           FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

           UserProfileChangeRequest request = new UserProfileChangeRequest.Builder()
                   .setPhotoUri(uri)
                   .build();


           firebaseUser.updateProfile(request)
                   .addOnSuccessListener(new OnSuccessListener<Void>() {
                       @Override
                       public void onSuccess(Void aVoid) {

                           Log.d(TAG, "onSuccess: profile image url updated");
                       }
                   })
                   .addOnFailureListener(new OnFailureListener() {
                       @Override
                       public void onFailure(@NonNull Exception e) {

                           Log.d(TAG, "onFailure: "+e.getCause());
                       }
                   });


    }
}