package com.aj.notesapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FirestoreExamples extends AppCompatActivity {


    //init instance of firestore
    FirebaseFirestore firestore = FirebaseFirestore.getInstance();

    private static final String TAG = "FirestoreExamples";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firestore_examples);
    }


    public void CreateDocument(View view) {

        //key is type string
        //values are type object beacuse these can be any type.. eg.string,bool,int etc
        Map<String,Object> map = new HashMap<>();
        //adding values to map object using put method
        map.put("name","Ajay Waghe");
        map.put("Age",24);
        map.put("isCrazy",true);
        map.put("created_at",new Timestamp(new Date()));


        firestore.collection("notes")
                .add(map)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {

                        Log.d(TAG, "onSuccess: Created Successfully");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        Log.e(TAG, "onFailure: "+ e.getLocalizedMessage());
                    }
                });

    }

    public void ReadDocument(View view) {

        firestore.collection("notes")
                //query
                .whereLessThan("Age",24)
                //get method to get data
                .get()
                //listeners to get data snapshots contain data
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                        Log.d(TAG, "onSuccess: Getting data success !");

                        //because getDocuments is type list so we store it in list
                        List<DocumentSnapshot> snapshotList = queryDocumentSnapshots.getDocuments();

                        //looping in snapshotList
                        for (DocumentSnapshot snapshot : snapshotList)
                        {
                            //getting all data available in collection->document
                            Log.d(TAG, "onSuccess: "+ snapshot.getData());
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        //get failed msg
                        Log.d(TAG, "onFailure: Failed getting data");
                    }
                });
    }

    public void UpdateDocument(View view) {


        //making document reference to access document id
        final  DocumentReference   docRef = firestore.collection("notes")
                                                      .document("OEXtmyzXRU35AGcZJqu5");


        //creating map for key value paires
        Map<String,Object> map = new HashMap<>();

        map.put("name","Tejal");
        map.put("isCrazy",false);


        //update() method to document Reference
        //update document with data map
        docRef.update(map)
                //adding listeners
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        Log.d(TAG, "onSuccess: yey.. Updated document..");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        Log.d(TAG, "onFailure: "+ e);
                    }
                });



    }

    public void DeleteDocument(View view) {

//deleteing document with id
        firestore.collection("notes")
                .document("wOVl4mHT1S2HTZBIBAND")
                .delete()

                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        Log.d(TAG, "onSuccess: Deleted doc");
                    }
                })

                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        Log.d(TAG, "onFailure: "+ e);
                    }
                });

    }

    public void getAllDocument(View view) {
//order by query
        firestore.collection("notes")
                .orderBy("isCrazy")
                .get()

                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                        if (queryDocumentSnapshots != null)
                        {
                            List<DocumentSnapshot> snapshotList = queryDocumentSnapshots.getDocuments();

                            for (DocumentSnapshot snapshot : snapshotList)
                            {
                                Log.d(TAG, "onSuccess: "+ snapshot.getData());
                            }
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        Log.d(TAG, "onFailure: "+e);
                    }
                });
    }

    public void getAllDocumentWithRealTimeUpdates(View view) {

        //to get realTime update of data changes in our documents we use this

        // DocumentSnapshot will get all documents data with changed one

//        firestore.collection("notes")
//                .addSnapshotListener(new EventListener<QuerySnapshot>() {
//                    @Override
//                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
//
//
//                        //first check for any exception
//                        //if any return
//                        if (e != null)
//                        {
//                            Log.d(TAG, "onEvent: "+ e);
//                            return;
//                        }
//
//                        //check queryDocumentSnapshot for not null
//                        if (queryDocumentSnapshots != null)
//                        {
//
//                            //store querySnapshot docs in list
//                            List<DocumentSnapshot> snapshotList = queryDocumentSnapshots.getDocuments();
//
//                            //loop through docs
//                            for (DocumentSnapshot snapshot : snapshotList)
//                            {
//
//                                //get data from snapshot of queryDocumentSnapshot
//                                Log.d(TAG, "onEvent: "+ snapshot.getData());
//                            }
//
//                        }
//                    }
//                });

        //*----------------------------------------------------------*//

        //to get realTime update of data changes in our documents we use this

        //DocumentChange will onlu return changed/updated document

        firestore.collection("notes")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {


                        //first check for any exception
                        //if any return
                        if (e != null)
                        {
                            Log.d(TAG, "onEvent: "+ e);
                            return;
                        }

                        //check queryDocumentSnapshot for not null
                        if (queryDocumentSnapshots != null)
                        {

                            //store querySnapshot docs in list
                            List<DocumentChange> snapshotList = queryDocumentSnapshots.getDocumentChanges();

                            //loop through docs
                            for (DocumentChange snapshot : snapshotList)
                            {

                                //get data from snapshot of queryDocumentSnapshot
                                Log.d(TAG, "onEvent: "+ snapshot.getDocument().getData());
                            }

                        }
                    }
                });
    }
}